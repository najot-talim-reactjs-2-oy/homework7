import PropTypes from "prop-types";
import './product.css'
import { useContext } from "react";
import { ProductContext } from "../../context/ProductContext";

const ProductCard = ({id, filter, name, image, description, price}) => {
  const { addToCart } = useContext(ProductContext);

  return (
    <div className="pr_card">
      <img src={image} alt="" />
      <div>
        {filter === "" ? "" : <p className="filter">{filter}</p>}
      </div>
      <div className='text-pr'>
        <h4 className='clamped-text'>{name}</h4>
        <p className='des clamped-text'>{description}</p>
        <div className='btnla'>
          <button onClick={() => addToCart(id)}> Add to cart {id}</button>
          <p>от {price} ₽</p>
        </div>
      </div>
    </div>
  );
};

ProductCard.propTypes = {
  id: PropTypes.number,
  filter: PropTypes.string,
  image: PropTypes.string,
  name: PropTypes.string,
  description: PropTypes.string,
  price: PropTypes.string
};

export default ProductCard;
